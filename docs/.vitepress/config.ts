import { mdPlugin } from "./config/plugin"
console.log(mdPlugin);

const buildTransformers = () => {
    const transformer = () => {
      return {
        props: [],
        needRuntime: true,
      }
    }
  
    const transformers = {}
    const directives = [
      'infinite-scroll',
      'loading',
      'popover',
      'click-outside',
      'repeat-click',
      'trap-focus',
      'mousewheel',
      'resize',
    ]
    directives.forEach((k) => {
      transformers[k] = transformer
    })
  
    return transformers
  }

module.exports = {
    title: 'Hello VitePress22222',
    description: 'Just playing around.',
    markdown: {
        lineNumbers: true,
        config: (md) => mdPlugin(md),
    },
    vue: {
        template: {
            ssr: true,
            compilerOptions: {
                directiveTransforms: buildTransformers(),
            },
        },
    },
}