
import type { UserConfig } from 'vitepress'
import { demoBlockPlugin } from 'vitepress-theme-demoblock'
import nav from './nav'
import sidebar from './sidebar'


export const config: UserConfig = {
  title: 'VitePressStudy',
  description: '学习VitePress',
  //解决你的站点部署不是在根目录的情况。
  base:'/VitepressStudy/',
  themeConfig: {
    // nav
    nav,

    // sidebar
    sidebar,

  },
  markdown: {
    // options for markdown-it-toc,标题目录
    toc: { includeLevel: [1, 2] },
    lineNumbers: true,
    config: (md) => {
      md.use(demoBlockPlugin)
    }
  }
}