const nav = [
    { text: '我的笔记', link: '/note/' },
    { text: '插件与配置', link: '/plugin/', items:[
        {text:"代码块插件",link:"https://gitee.com/theShyboy/sunhshine-vitepress-theme-demoblock?_from=gitee_search"}
    ] },
    {
        text: '参考文档', items: [
            { text: '中文文档', link: 'https://fttp.jjf-tech.cn/vitepress/' }, // 可不写后缀 .md
            { text: 'github', link: 'https://github.com/vuejs/vitepress' }// 外部链接
        ]
    }
]
export default nav;
